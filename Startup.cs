
using Microsoft.AspNetCore.Authentication.Cookies;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Builder;
using Microsoft.AspNetCore.Hosting;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.HttpsPolicy;
using Microsoft.AspNetCore.Identity;
using Microsoft.AspNetCore.Mvc.Authorization;
using Microsoft.AspNetCore.Server.IISIntegration;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.Extensions.Hosting;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Threading.Tasks;
using SQLaccess;
using Email;
using static OracleAccess.OracleDataAccess;
using OracleAccess;
using Encrypt;
using SignalRChat.Hubs;
using ClearCom;
using System.Text.Json;
using System.Text.Json.Serialization;

namespace WebApplication3
{
    public class Startup
    {
        private readonly IConfiguration _configuration;
        public Startup(IConfiguration configuration, IWebHostEnvironment evm)
        {
            Configuration = configuration;
            _configuration = configuration;
            var builder = new ConfigurationBuilder()
               .SetBasePath(evm.ContentRootPath)
               .AddJsonFile("appsettings.json", true, true)
               .AddJsonFile($"appsettings.{evm.EnvironmentName}.json", true)
               .AddEnvironmentVariables();
            Configuration = builder.Build(); // load all file config to Configuration property 
            AppSettings.Instance.SetConfiguration(Configuration);
        }

        public IConfiguration Configuration { get; }

        // This method gets called by the runtime. Use this method to add services to the container.
        public void ConfigureServices(IServiceCollection services)
        {
            services.AddControllersWithViews();
            #region Authentication

            services.AddScoped<IAuthenticationService, LdapAuthenticationService>();
            //services.AddScoped<IEncryptionService, EncryptionService>();


            //services.AddAuthentication(IISDefaults.AuthenticationScheme);
            //services.AddAuthentication(CookieAuthenticationDefaults.AuthenticationScheme).AddCookie(o => o.LoginPath = new PathString("/"));

            ////services.AddIdentity<User, Role>(options =>
            ////{
            ////    options.User.AllowedUserNameCharacters = "abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ0123456789-_\\";
            ////}).AddEntityFrameworkStores<VIPADBContext>();
            ///


            services.ConfigureApplicationCookie(options =>
            {
                options.AccessDeniedPath = new PathString("/Security/LoginForm");
                options.Cookie.Name = CookieAuthenticationDefaults.AuthenticationScheme;
                options.ExpireTimeSpan = TimeSpan.FromMinutes(60);
                options.LoginPath = new PathString("/Security/LoginForm");
                options.SlidingExpiration = true;
            });

            services.AddSession(options =>
            {
                options.IdleTimeout = TimeSpan.FromMinutes(60);
            });

            services.AddAuthentication(CookieAuthenticationDefaults.AuthenticationScheme)
                .AddCookie(CookieAuthenticationDefaults.AuthenticationScheme, options =>
                {
                    options.AccessDeniedPath = "/Security/LoginForm";
                    options.LoginPath = new PathString("/Security/LoginForm"); ;
                });
            #endregion

            services.Add(new ServiceDescriptor(typeof(ISQLaccess), new dbSQL()));
            //services.Add(new ServiceDescriptor(typeof(IOracleaccess), new OracleDataAccess()));
            services.Add(new ServiceDescriptor(typeof(IEncryptionService), new Encryptor()));


            services.Configure<MailSettings>(Configuration.GetSection("MailSettings"));
            services.Configure<EncryptorSettings>(Configuration.GetSection("EncryptorSettings"));
            services.Configure<LdapConfig>(Configuration.GetSection("Ldap"));

            services.AddSignalR();
            services.AddSingleton<ChatHub>();
            //services.AddHostedService<MigratorHostedService>();
            services.AddMvc().AddNewtonsoftJson();
            services.AddControllers().AddNewtonsoftJson();
            services.AddControllersWithViews().AddNewtonsoftJson();
            services.AddRazorPages().AddNewtonsoftJson();
        }


        // This method gets called by the runtime. Use this method to configure the HTTP request pipeline.
        public void Configure(IApplicationBuilder app, IWebHostEnvironment env)
        {
            if (env.IsDevelopment())
            {
                app.UseDeveloperExceptionPage();
            }
            else
            {
                app.UseExceptionHandler("/Home/Error");
                // The default HSTS value is 30 days. You may want to change this for production scenarios, see https://aka.ms/aspnetcore-hsts.
                app.UseHsts();
            }
            app.UseHttpsRedirection();
            app.UseStaticFiles();

            app.UseRouting();

            app.UseAuthentication();
            app.UseAuthorization();

            app.UseEndpoints(endpoints =>
            {
                endpoints.MapControllerRoute(
                    name: "default",
                    pattern: "{controller=Home}/{action=Index}/{id?}");
                endpoints.MapHub<ChatHub>("/chatHub");
            });
        }

    }
}
