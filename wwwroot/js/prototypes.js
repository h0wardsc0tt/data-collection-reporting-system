﻿//Number.prototype.format(n, x) 
//param integer n: length of decimal
//param integer x: length of sections
//$('#alerteModal').modal('hide');
Number.prototype.format = function (n, x) {
    var re = '\\d(?=(\\d{' + (x || 3) + '})+' + (n > 0 ? '\\.' : '$') + ')';
    return this.toFixed(Math.max(0, ~~n)).replace(new RegExp(re, 'g'), '$&,');
};
jQuery.fn.center = function () {
    this.css("position", "absolute");
    this.css("top", Math.max(0, (($(window).height() - $(this).outerHeight()) / 2) +
                                                $(window).scrollTop()) + "px");
    this.css("left", Math.max(0, (($(window).width() - $(this).outerWidth()) / 2) +
                                                $(window).scrollLeft()) + "px");
    return this;
}
Array.prototype.insert = function (index, item) {
    this.splice(index, 0, item);
};

Array.prototype.delete = function (index) {
    this.splice(index, 1);
};

Array.prototype.clone = function () {
    return JSON.parse(JSON.stringify(this));
};

String.prototype.toCamelCase = function () {
    return this.toLowerCase().replace(/\b[a-z]/g, function (letter) {
        return letter.toUpperCase();
    });
};

Array.prototype.Sort = function (order, type, key) {
    switch (type) {
        case 'string':
            var xx = 'rrrr'.toUpperCase();
            if (order == 'asc') {
                this.sort(function (a, b) {
                    var textA = a[key].toUpperCase();
                    var textB = b[key].toUpperCase();
                    return (textA < textB) ? -1 : (textA > textB) ? 1 : 0;
                });
            } else {
                this.sort(function (a, b) {
                    var textA = a[key].toUpperCase();
                    var textB = b[key].toUpperCase();
                    return (textA > textB) ? -1 : (textA < textB) ? 1 : 0;
                });

            }
            break
        case 'numeric':
            if (order == 'asc') {
                this.sort(function (a, b) {
                    var textA = Number(a[key]);
                    var textB = Number(b[key]);
                    return (textA < textB) ? -1 : (textA > textB) ? 1 : 0;
                });
            } else {
                this.sort(function (a, b) {
                    var textA = Number(a[key]);
                    var textB = Number(b[key]);
                    return (textA > textB) ? -1 : (textA < textB) ? 1 : 0;
                });

            }
            break
        case 'int':
            if (order == 'asc') {
                this.sort(function (a, b) {
                    var textA = a[key] == 'N/A' || a[key] == '' ? 0 : parseInt(a[key]);
                    var textB = b[key] == 'N/A' || b[key] == '' ? 0 : parseInt(b[key]);
                    return (textA < textB) ? -1 : (textA > textB) ? 1 : 0;
                });
            } else {
                this.sort(function (a, b) {
                    var textA = a[key] == 'N/A' || a[key] == '' ? 0 : parseInt(a[key]);
                    var textB = b[key] == 'N/A' || b[key] == ''  ? 0 : parseInt(b[key]);
                    return (textA > textB) ? -1 : (textA < textB) ? 1 : 0;
                });

            } break
        case 'float':
            if (order == 'asc') {
                this.sort(function (a, b) {
                    //console.log(a[key] + '    ' + b[key])
                    var textA = parseFloat(a[key]);
                    var textB = parseFloat(b[key]);
                    return (textA < textB) ? -1 : (textA > textB) ? 1 : 0;
                });
            } else {
                this.sort(function (a, b) {
                    //console.log(a[key] + '    ' + b[key])
                    var textA = parseFloat(a[key]);
                    var textB = parseFloat(b[key]);
                    return (textA > textB) ? -1 : (textA < textB) ? 1 : 0;
                });

            } break
        case 'percent':
            if (order == 'asc') {
                this.sort(function (a, b) {
                    //console.log(a[key] + '    ' + b[key])
                    var textA = parseFloat(a[key].replace(' %', ''));
                    var textB = parseFloat(b[key].replace(' %', ''));
                    return (textA < textB) ? -1 : (textA > textB) ? 1 : 0;
                });
            } else {
                this.sort(function (a, b) {
                    //console.log(a[key] + '    ' + b[key])
                    var textA = parseFloat(a[key].replace(' %', ''));
                    var textB = parseFloat(b[key].replace(' %', ''));
                    return (textA > textB) ? -1 : (textA < textB) ? 1 : 0;
                });

            } break

        case 'date':
            if (order == 'asc') {
                this.sort(function (a, b) {
                    var textA = new Date(a[key]);
                    var textB = new Date(b[key]);
                    return (textA < textB) ? -1 : (textA > textB) ? 1 : 0;
                });
            } else {
                this.sort(function (a, b) {
                    var textA = new Date(a[key]);
                    var textB = new Date(b[key]);
                    return (textA > textB) ? -1 : (textA < textB) ? 1 : 0;
                });

            }
            break
    }
    //return this.sort((a, b) => Number(b.storenumber) - Number(a.storenumber));
};
$.date = function () {
    var d = new Date();
    var day = d.getDate();
    var month = d.getMonth() + 1;
    var year = d.getFullYear();
    if (day < 10) {
        day = "0" + day;
    }
    if (month < 10) {
        month = "0" + month;
    }
    var date = month + "/" + day + "/" + year;

    return date;
};