﻿// Please see documentation at https://docs.microsoft.com/aspnet/core/client-side/bundling-and-minification
// for details on configuring this project to bundle and minify static web assets.

// Write your JavaScript code.
Date.prototype.addDays = function (days) {
    this.setDate(this.getDate() + parseInt(days));
    return this;
};

$(document).ready(function () {

    // datacollectionList.fpy.Sort('desc', 'percent', 'FPY_ATE');
    //console.log(datacollectionList);
    //alert($(this).width())
    function reorient(e) {
        var portrait = (window.orientation % 180 == 0);
        console.log(portrait)
        //$("body > div").css("-webkit-transform", !portrait ? "rotate(-90deg)" : "");
        $("body > div").css("-webkit-transform", !portrait ? "rotate(90deg)" : "");
    }
    //window.onorientationchange = reorient;
    //window.setTimeout(reorient, 0);

    //$(window)
    //    .bind('orientationchange', function () {
    //        alert(screen.orientation.angle)
    //        if (window.orientation % 180 == 0) {
    //            $(document.body).css("-webkit-transform-origin", "")
    //                .css("-webkit-transform", "");
    //        }
    //        else {
    //            if (window.orientation > 0) { //clockwise
    //                $(document.body).css("-webkit-transform-origin", "200px 190px")
    //                    .css("-webkit-transform", "rotate(-90deg)");
    //            }
    //            else {
    //                $(document.body).css("-webkit-transform-origin", "280px 190px")
    //                    .css("-webkit-transform", "rotate(90deg)");
    //            }
    //        }
    //    })
    //.trigger('orientationchange');

    //window.addEventListener("orientationchange", function () {
    //    // Announce the new orientation number
    //    //alert(screen.orientation.angle)
    //    //alert('ffff  '+screen.orientation.angle)
    //    if (window.orientation % 180 == 0) {
    //        //alert('1')
    //        //window.orientation.lock('landscape')
    //        screen.orientation.lock('landscape')
    //        $(document.body).css("-webkit-transform-origin", "")
    //            .css("-webkit-transform", "");
    //    }
    //    else {
    //        if (window.orientation > 0) { //clockwise
    //            //alert('2')
    //            $(document.body).css("-webkit-transform-origin")//, "200px 190px"
    //                .css("-webkit-transform");//, "rotate(-45deg)"
    //        }
    //        else {
    //            //alert('3')
    //            $(document.body).css("-webkit-transform-origin")//, "280px 190px"
    //                .css("-webkit-transform");//, "rotate(90deg)"
    //        }
    //    }
    //}, false);

    $(function () {
        if (window.matchMedia('(max-width: 768px)').matches) {
            //alert('fffffff');
            //screen.lockOrientation('landscape');
            //window.screen.orientation.lock('landscape');
            // window.orientation.lock('landscape')
            //     screen.orientation.lock('landscape')
            //     screen.orientation.lock('landscape').then(res => alert(res)).catch(err => alert.log(err))
        }
        //screen.onorientationchange
        /*screen.orientation.lock('landscape')*/
        console.log(screen.orientation)
        //alert(screen.orientation.angle)

        //datetimepicker
        $(".FirstPassStartDate").datetimepicker({
            dateFormat: 'mm/dd/yy'
            , timeFormat: 'hh:mm:ss TT'
        });
        $(".FirstPassEndDate").datetimepicker({
            dateFormat: 'mm/dd/yy'
            , timeFormat: 'hh:mm:ss TT'
        });
        $(".FailureStartDate").datetimepicker({
            dateFormat: 'mm/dd/yy'
            , timeFormat: 'hh:mm:ss TT'
        });
        $(".FailureEndDate").datetimepicker({
            dateFormat: 'mm/dd/yy'
            , timeFormat: 'hh:mm:ss TT'
        });
        $(".cpkStartDate").datetimepicker({
            dateFormat: 'mm/dd/yy'
            , timeFormat: 'hh:mm:ss TT'
        });
        $(".cpkEndDate").datetimepicker({
            dateFormat: 'mm/dd/yy'
            , timeFormat: 'hh:mm:ss TT'
        });
        $(".dummy1").datetimepicker({
            dateFormat: 'mm/dd/yy'
            , timeFormat: 'hh:mm:ss TT'
        });
        $(".dummy2").datetimepicker({
            dateFormat: 'mm/dd/yy'
            , timeFormat: 'hh:mm:ss TT'
        });
        $(".dummy3").datetimepicker({
            dateFormat: 'mm/dd/yy'
            , timeFormat: 'hh:mm:ss TT'
        });
        var date = new Date();
        var datePlusOne = new Date();
        datePlusOne.setDate(date.getDate() + parseInt(1));
        //$(".FirstPassStartDate").datetimepicker("setDate", date);
        //$(".FirstPassEndDate").datetimepicker("setDate", datePlusOne);
        //$(".FailureStartDate").datetimepicker("setDate", date);
        //$(".FailureEndDate").datetimepicker("setDate", datePlusOne);
        //$(".cpkStartDate").datetimepicker("setDate", date);
        //$(".cpkEndDate").datetimepicker("setDate", datePlusOne);

        $('.dummy1').datetimepicker('setDate', (new Date($.datepicker.formatDate('yy', date), date.getMonth(), $.datepicker.formatDate('dd', date), 12, 00, 00, 'AM')));
        $('.dummy2').datetimepicker('setDate', (new Date($.datepicker.formatDate('yy', date), date.getMonth(), $.datepicker.formatDate('dd', date), 12, 00, 00, 'AM')));
        $('.dummy3').datetimepicker('setDate', (new Date($.datepicker.formatDate('yy', date), date.getMonth(), $.datepicker.formatDate('dd', date), 12, 00, 00, 'AM')));


        $('.FirstPassStartDate').datetimepicker('setDate', (new Date($.datepicker.formatDate('yy', date), date.getMonth(), $.datepicker.formatDate('dd', date), 12, 00, 00, 'AM')));
        $('.FirstPassEndDate').datetimepicker('setDate', (new Date($.datepicker.formatDate('yy', date), date.getMonth(), $.datepicker.formatDate('dd', date), 23, 59, 59)));
        $('.FailureStartDate').datetimepicker('setDate', (new Date($.datepicker.formatDate('yy', date), date.getMonth(), $.datepicker.formatDate('dd', date), 12, 00, 00, 'AM')));
        $('.FailureEndDate').datetimepicker('setDate', (new Date($.datepicker.formatDate('yy', date), date.getMonth(), $.datepicker.formatDate('dd', date), 23, 59, 59)));
        $('.cpkStartDate').datetimepicker('setDate', (new Date($.datepicker.formatDate('yy', date), date.getMonth(), $.datepicker.formatDate('dd', date), 12, 00, 00, 'AM')));
        $('.cpkEndDate').datetimepicker('setDate', (new Date($.datepicker.formatDate('yy', date), date.getMonth(), $.datepicker.formatDate('dd', date), 23, 59, 59)));
    });

    $('.failures .fa-search').on('click', function () {
        $('.failures .fa-spin').show();
        $.ajax({
            url: '/Home/Failures/?firstPassStartDate=' + Formatdate($('.FailureStartDate').val()) + '&firstPassEndDate=' + Formatdate($('.FailureEndDate').val()) + '&partnumber=' + $('.failure-partnumber').val(),
            type: "GET",
            contentType: "application/json; charset=utf-8",
            dataType: "json",
            success: function (dto) {
                if (dto.status == 'OK') {
                    datacollectionList.failures = dto.failures;
                    var split = datacollectionList.failuresSort.split(';');
                    datacollectionList.failures.Sort(split[1], split[2], split[0]);
                    console.log(datacollectionList.failures);
                    var update = '';
                    var tmp = '';
                    $.each(datacollectionList.failures, function (i, item) {
                        item.testStep = item.testStep.replace(/TC0/g, '').replace(/TC/g, '');
                    });
                    $('.failures .failures-table tbody').html('');
                    $.each(datacollectionList.failures, function (i, item) {
                        tmp = '<tr>';
                        tmp += '<td>' + item.testType + '</td>';
                        tmp += '<td style="text-align:left">' + item.partNum + '</td>';
                        tmp += '<td>' + item.partRev + '</td>';
                        tmp += '<td>' + item.serialNum + '</td>';
                        tmp += '<td>' + item.testStep + '</td>';
                        tmp += '<td style="text-align:left">' + item.testName + '</td>';
                        tmp += '<td>' + item.pass_Fail + '</td>';
                        tmp += '<td>' + item.lowerLimit + '</td>';
                        tmp += '<td>' + item.upperLimit + '</td>';
                        tmp += '<td>' + item.measValue + '</td>';
                        tmp += '<td>' + item.units + '</td>';
                        tmp += '<td>' + item.testDate + '</td>';
                        tmp += '</tr>';
                        update += tmp;
                    });
                    $('.failures .failures-table tbody').append(update);
                    $('.failures .fa-spin').hide();
                    $('.fpy .fa-spin').hide();
                }
            },
            error: function (response) {
            }
        });

    });

    $('.fpy .fa-search').on('click', function () {
        var dto = {};
        dto.firstPassStartDate = Formatdate($('.fpy .FirstPassStartDate').val());
        dto.firstPassEndDate = Formatdate($('.fpy .FirstPassEndDate').val());
        $('.fpy .fa-spin').show();
        $.ajax({
            url: '/Home/FPY/?firstPassStartDate=' + Formatdate($('.fpy .FirstPassStartDate').val()) + '&firstPassEndDate=' + Formatdate($('.fpy .FirstPassEndDate').val()),
            type: "GET",
            //data: { firstPassStartDate: Formatdate($('.FirstPassStartDate').val()), firstPassEndDate: Formatdate($('.FirstPassEndDate').val()) },//JSON.stringify(dto),
            contentType: "application/json; charset=utf-8",
            dataType: "json",
            success: function (dto) {
                if (dto.status == 'OK') {
                    datacollectionList.fpy = dto.fpy;
                    var split = datacollectionList.fpySort.split(';');
                    datacollectionList.fpy.Sort(split[1], split[2], split[0]);

                    //datacollectionList.fpy.Sort(direction, stype, elm)
                    //datacollectionList.fpySort = elm + ';' + direction + ';' + stype

                    //direction, stype, elm

                    console.log(dto);
                    console.log(datacollectionList)
                    var fpyUpdate = '';
                    var tmp = '';
                    $('.fpy .fpy-table tbody').html('');
                    $.each(datacollectionList.fpy, function (i, item) {
                        var pn = item.partNum.replace(/ /g, '');
                        var link = '<a class="pnLink" pn="' + item.partNum.replace(/ /g, '') + '" href="#">' + pn + '</a>';//" onclick="return failures("' + pn + '");
                        tmp = '<tr>';
                        tmp += '<td>' + item.testType + '</td>';
                        tmp += '<td style = "text-align:left">' + link + '</td>';
                        tmp += '<td>' + item.partRev + '</td>';
                        tmp += '<td style = "text-align:left">' + item.partDesc.replace('~', "'") + '</td>';
                        tmp += '<td>' + item.passQty + '</td>';
                        tmp += '<td>' + item.failQty + '</td>';
                        tmp += '<td>' + item.totalQty + '</td>';
                        tmp += '<td>' + item.fpyate + '</td>';
                        tmp += '</tr>';
                        fpyUpdate += tmp;
                    });
                    $('.fpy .fpy-table tbody').append(fpyUpdate);
                    ln_link();
                    $('.fpy .fa-spin').hide();

                }
            },
            error: function (response) {
            }
        });
    });

    $('.cpk .fa-search').on('click', function () {
        var dto = {};
        $('.cpk .fa-spin').show();

        $.ajax({
            url: '/Home/Cpk/?firstPassStartDate=' + Formatdate($('.cpk .cpkStartDate').val()) + '&firstPassEndDate=' + Formatdate($('.cpk .cpkEndDate').val()) + '&partnumber=' + ($('.cpk .cpk-partnumber').val() == '' ? null : $('.cpk .cpk-partnumber').val()),
            type: "GET",
            //data: { firstPassStartDate: Formatdate($('.FirstPassStartDate').val()), firstPassEndDate: Formatdate($('.FirstPassEndDate').val()) },//JSON.stringify(dto),
            contentType: "application/json; charset=utf-8",
            dataType: "json",
            success: function (dto) {
                if (dto.status == 'OK') {
                    datacollectionList.cpk = dto.cpk;
                    console.log(datacollectionList.cpk);
                    var update = '';
                    var tmp = '';
                    $('.cpk .cpk-table tbody').html('');
                    $.each(datacollectionList.cpk, function (i, item) {
                        tmp = '<tr>';
                        tmp += '<td style="text-align:left">' + item.partNum + '</td>';
                        tmp += '<td>' + item.testStep + '</td>';
                        tmp += '<td style = "text-align:left">' + item.testName + '</td>';
                        tmp += '<td>' + item.ll + '</td>';
                        tmp += '<td>' + item.ul + '</td>';
                        tmp += '<td>' + item.units + '</td>';
                        tmp += '<td>' + item.cpk + '</td>';
                        tmp += '</tr>';
                        update += tmp;
                    });
                    $('.cpk .cpk-table tbody').append(update);
                    $('.cpk .fa-spin').hide();
                }
            },
            error: function (response) {
            }
        });
    });


    //var dto = {};
    //dto.firstPassStartDate = 'ttttt';
    //dto.firstPassEndDate = 'bbbbbbb';
    //$.ajax({
    //    url: '/Home/Test',
    //    type: "POST",
    //    data: JSON.stringify(dto),
    //    contentType: "application/json; charset=utf-8",
    //    dataType: "json",
    //    success: function (dto) {
    //        if (dto.status == 'OK') {
    //        }
    //    },
    //    error: function (response) {
    //    }
    //});
    sortevents();
});

function sortevents() {
    $('th span.asc').on('click', function (e) {
        switch (e.type) {
            case 'click':
                var obj = $(this).attr('obj')
                //datacollectionList[obj].Sort('desc', $(this).attr('stype'), $(this).attr('elm'))
                dosort(obj, 'desc', $(this).attr('stype'), $(this).attr('elm'))
                $(this).removeClass('asc').addClass('desc');
                $('th span.asc').unbind("click").unbind("mouseover");
                $('th span.desc').unbind("click").unbind("mouseover");
                $('th span.sort').unbind("click").unbind("mouseover");
                sortevents();
                tbodtBuild(obj);
                break;
            case 'mouseover':
            //this.style.cursor = 'pointer';
            //break;
        }
    });
    $('th span.desc').on('click', function (e) {
        switch (e.type) {
            case 'click':
                var obj = $(this).attr('obj')
                //datacollectionList[obj].Sort('asc', $(this).attr('stype'), $(this).attr('elm'))
                dosort(obj, 'asc', $(this).attr('stype'), $(this).attr('elm'))
                $(this).removeClass('desc').addClass('asc');
                $('th span.asc').unbind("click").unbind("mouseover");
                $('th span.desc').unbind("click").unbind("mouseover");
                $('th span.sort').unbind("click").unbind("mouseover");
                sortevents();
                tbodtBuild(obj);
                break;
            //case 'mouseover':
            //    this.style.cursor = 'pointer';
            //    break;
        }
    });
    $('th span.sort').on('click', function (e) {
        switch (e.type) {
            case 'click':
                var obj = $(this).attr('obj')
                $('.' + obj + '-table th span.sortable').removeClass('desc').removeClass('asc').addClass('sort');
                dosort(obj, 'desc', $(this).attr('stype'), $(this).attr('elm'))
                $(this).removeClass('sort').addClass('desc');
                $('th span.asc').unbind("click").unbind("mouseover");
                $('th span.desc').unbind("click").unbind("mouseover");
                $('th span.sort').unbind("click").unbind("mouseover");
                sortevents();
                tbodtBuild(obj);
                break;
            //case 'mouseover':
            //    this.style.cursor = 'pointer';
            //    break;
        }
    });
}

function failures(PartNum) {
    $('.FailureStartDate').val($('.FirstPassStartDate').val());
    $('.FailureEndDate').val($('.FirstPassEndDate').val());
    $.ajax({
        url: '/Home/Failures/?firstPassStartDate=' + Formatdate($('.FailureStartDate').val()) + '&firstPassEndDate=' + Formatdate($('.FailureEndDate').val()) + '&partnumber=' + PartNum.trim(),
        type: "GET",
        contentType: "application/json; charset=utf-8",
        dataType: "json",
        success: function (dto) {
            if (dto.status == 'OK') {
                datacollectionList.failures = dto.failures;
                console.log(datacollectionList.failures);
                var update = '';
                var tmp = '';
                $('.failures .failures-table tbody').html('');
                $.each(datacollectionList.failures, function (i, item) {
                    tmp = '<tr>';
                    tmp += '<td>' + item.testType + '</td>';
                    tmp += '<td style="text-align:left">' + item.partNum + '</td>';
                    tmp += '<td>' + item.partRev + '</td>';
                    tmp += '<td>' + item.serialNum + '</td>';
                    tmp += '<td>' + item.testStep + '</td>';
                    tmp += '<td style="text-align:left">' + item.testName + '</td>';
                    tmp += '<td>' + item.pass_Fail + '</td>';
                    tmp += '<td>' + item.lowerLimit + '</td>';
                    tmp += '<td>' + item.upperLimit + '</td>';
                    tmp += '<td>' + item.measValue + '</td>';
                    tmp += '<td>' + item.units + '</td>';
                    tmp += '<td>' + item.testDate + '</td>';
                    tmp += '</tr>';
                    update += tmp;
                });
                $('.failures .failures-table tbody').append(update);
                $('.fpy').hide();
                $('.failures').show();
                $('.fpy .fa-spin').hide();
                $('.failure-partnumber').val(PartNum.trim());
            }
        },
        error: function (response) {
        }
    });
    return false;
}

function dosort(obj, direction, stype, elm) {
    count = 0;
    obj_1 = [];
    obj_2 = [];
    obj_3 = [];
    switch (obj) {
        case 'fpy':
            datacollectionList.fpy.Sort(direction, stype, elm)
            datacollectionList.fpySort = elm + ';' + direction + ';' + stype
            //console.log(datacollectionList)
            break;
        case 'cpk':
            datacollectionList.cpk.Sort(direction, stype, elm)
            datacollectionList.cpkSort = elm + ';' + direction + ';' + stype
            //console.log(datacollectionList)
            break;
        case 'failures':
            switch (elm) {
                case 'lowerLimit':
                case 'upperLimit':
                case 'measValue':
                //case 'testStep':
                    count = datacollectionList.failures.length
                    row = 0;
                    for (i = 0; i < count; i++) {
                        if (datacollectionList.failures[i][elm] != '' && datacollectionList.failures[i][elm] != 'N/A') {
                            console.log(datacollectionList.failures[i]);
                            if ($.isNumeric(datacollectionList.failures[i][elm])) {
                                obj_1.insert(row, datacollectionList.failures[i]);
                                row++;
                            }
                        }
                    }
                    obj_1.Sort(direction, 'float', elm)
                    row = 0;
                    for (i = 0; i < count; i++) {
                        if (datacollectionList.failures[i][elm] != '' && !$.isNumeric(datacollectionList.failures[i][elm])) {
                            console.log(datacollectionList.failures[i]);
                            obj_2.insert(row, datacollectionList.failures[i]);
                            row++;
                        }
                    }
                    obj_2.Sort(direction, stype, elm)
                    row = 0;
                    for (i = 0; i < count; i++) {
                        if (datacollectionList.failures[i][elm] == '') {
                            obj_3.insert(row, datacollectionList.failures[i]);
                            row++;
                        }
                    }

                    datacollectionList.failures = [];

                    row = 0;
                    for (i = 0; i < obj_1.length; i++) {
                        datacollectionList.failures.insert(row, obj_1[i]);
                        row++;
                    }
                    for (i = 0; i < obj_2.length; i++) {
                        datacollectionList.failures.insert(row, obj_2[i]);
                        row++;
                    }
                    for (i = 0; i < obj_3.length; i++) {
                        datacollectionList.failures.insert(row, obj_3[i]);
                        row++;
                    }
                    break;
                default:
                    datacollectionList.failures.Sort(direction, stype, elm)
                    break;
            }
            break;
    }
}

function tbodtBuild(obj) {
    var update = '';
    var tmp = '';
    switch (obj) {
        case 'fpy':
            $('.fpy .fpy-table tbody').html('');
            $.each(datacollectionList.fpy, function (i, item) {
                var pn = item.partNum.replace(/ /g, '');
                var link = '<a class="pnLink" pn="' + item.partNum.replace(/ /g, '') + '" href="#">' + pn + '</a>';//" onclick="return failures("' + pn + '");
                tmp = '<tr>';
                tmp += '<td>' + item.testType + '</td>';
                tmp += '<td style = "text-align:left">' + link + '</td>';
                tmp += '<td>' + item.partRev + '</td>';
                tmp += '<td style = "text-align:left">' + item.partDesc.replace('~', "'") + '</td>';
                tmp += '<td>' + item.passQty + '</td>';
                tmp += '<td>' + item.failQty + '</td>';
                tmp += '<td>' + item.totalQty + '</td>';
                tmp += '<td>' + item.fpyate + '</td>';
                tmp += '</tr>';
                update += tmp;
            });
            $('.fpy .fpy-table tbody').append(update);
            ln_link();
            break;
        case 'failures':
            $('.failures .failures-table tbody').html('');
            $.each(datacollectionList.failures, function (i, item) {
                tmp = '<tr>';
                tmp += '<td>' + item.testType + '</td>';
                tmp += '<td style="text-align:left">' + item.partNum + '</td>';
                tmp += '<td>' + item.partRev + '</td>';
                tmp += '<td>' + item.serialNum + '</td>';
                tmp += '<td>' + item.testStep + '</td>';
                tmp += '<td style="text-align:left">' + item.testName + '</td>';
                tmp += '<td>' + item.pass_Fail + '</td>';
                tmp += '<td>' + item.lowerLimit + '</td>';
                tmp += '<td>' + item.upperLimit + '</td>';
                tmp += '<td>' + item.measValue + '</td>';
                tmp += '<td>' + item.units + '</td>';
                tmp += '<td>' + item.testDate + '</td>';
                tmp += '</tr>';
                update += tmp;
            });
            $('.failures .failures-table tbody').append(update);
            ln_link();
            break;
        case 'cpk':
            $('.cpk .cpk-table tbody').html('');
            $.each(datacollectionList.cpk, function (i, item) {
                tmp = '<tr>';
                tmp += '<td style="text-align:left">' + item.partNum + '</td>';
                tmp += '<td>' + item.testStep + '</td>';
                tmp += '<td style = "text-align:left">' + item.testName + '</td>';
                tmp += '<td>' + item.ll + '</td>';
                tmp += '<td>' + item.ul + '</td>';
                tmp += '<td>' + item.units + '</td>';
                tmp += '<td>' + item.cpk + '</td>';
                tmp += '</tr>';
                update += tmp;
            });
            $('.cpk .cpk-table tbody').append(update);
            ln_link();
            break;
    }
}

function ln_link() {
    $('.pnLink').on('click', function () {
        $('.fpy .fa-spin').show();
        failures($(this).attr('pn'));
    });
}

function Formatdate(dt) {
    //var c1 = dt.split(' ');
    //var cc = c1[0].split('/');
    //return cc[1] + '/' + cc[0] + '/' + cc[2] + ' ' + c1[1] + ' ' + c1[2];
    return dt;

}
