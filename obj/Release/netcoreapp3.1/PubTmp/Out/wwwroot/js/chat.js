﻿"use strict";
var tryingToReconnect = false;
var chat;
var chatid;
var connection = new signalR.HubConnectionBuilder().withUrl("/chatHub").build();

connection.on("ReceiveMessage", function (user, message) {
  
});

connection.on("dbBroadcast", function (posttype, table, data) {
    var updatecell = JSON.parse(data);
    console.log(updatecell)
});

async function start() {
    try {
        console.log("SignalR Connecting.");
        await connection.start();
        chatid = connection.connectionId;
        console.log("SignalR Connected. " + chatid);

    } catch (err) {
        console.log(err);
        setTimeout(start, 5000);
    }
};

connection.onclose(async () => {
    await start();
});

// Start the connection.
start();
