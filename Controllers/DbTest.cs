﻿using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Configuration;
using System;
using WebApplication3.Models;


namespace WebApplication3.Controllers
{
    public class DbTest : Controller
    {
        private readonly IConfiguration _configuration;
        //private readonly IConfiguration _CustomAppSetting;
        public DbTest(IConfiguration configuration)
        {
            _configuration = configuration ?? throw new ArgumentNullException(nameof(configuration));

            //_CustomAppSetting = CustomAppSetting ?? throw new ArgumentNullException(nameof(CustomAppSetting));
        }
        public IActionResult Index()
        {
            sqlModel sqlModel = new sqlModel(_configuration);
            sqlModel.sqltest();
            sqlModel.ConfModel configurations = new sqlModel.ConfModel
            {
                configuration = _configuration
            };
            return View(configurations);
        }
    }
}
