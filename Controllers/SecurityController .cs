﻿using Microsoft.AspNetCore.Authentication;
using Microsoft.AspNetCore.Authentication.Cookies;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Security.Claims;
using System.Threading.Tasks;


namespace WebApplication3
{
    //[Route("[controller]/[action]")]
    public class SecurityController : Controller
    {
        private readonly IAuthenticationService authService;

        public SecurityController(IAuthenticationService authService)
        {
            this.authService = authService;
        }
        //[HttpPost]
        //public async Task<User> Login()
        //{
        //    User user = authService.Login("hscott", "July2021!");
        //    if (null != user)
        //    {
        //        var claims = new[] { new Claim(ClaimTypes.Name, "john"),

        //        new Claim("SecurityStamp ", Guid.NewGuid().ToString()),
        //        new Claim(ClaimTypes.Role, "Admin","User") };

        //        var claimsIdentity = new ClaimsIdentity(claims, CookieAuthenticationDefaults.AuthenticationScheme);

        //        var principal = new ClaimsPrincipal();
        //        principal.AddIdentity(claimsIdentity);

        //        await HttpContext.SignInAsync(CookieAuthenticationDefaults.AuthenticationScheme, principal);
        //    }

        //    return user;
        //}

        public async Task<ActionResult> UserLogin(LoginViewModel model, string returnUrl = null)
        {
            User user = authService.Login(model.Username.Split('@')[0], model.Password, returnUrl);
            if (user != null)
            {
                var claims = new[] { new Claim(ClaimTypes.Name, user.DisplayName),
                    new Claim("SecurityStamp ", Guid.NewGuid().ToString()),
                    new Claim(ClaimTypes.Role, "Admin"),
                    new Claim(ClaimTypes.Role, "User")
                };

                var claimsIdentity = new ClaimsIdentity(claims, CookieAuthenticationDefaults.AuthenticationScheme);

                var principal = new ClaimsPrincipal();
                principal.AddIdentity(claimsIdentity);

                await HttpContext.SignInAsync(CookieAuthenticationDefaults.AuthenticationScheme, principal);
                if (string.IsNullOrEmpty(returnUrl))
                {
                    Uri uri = new Uri(Request.GetTypedHeaders().Referer.ToString());
                    return Redirect(uri.LocalPath);
                }
                return Redirect(returnUrl);
            }

            return Redirect("/Security/LoginForm/?returnUrl=" + returnUrl + "&error=login_error");
        }


        public async Task<IActionResult> Logout()
        {
            await HttpContext.SignOutAsync(CookieAuthenticationDefaults.AuthenticationScheme);

            return Redirect("/");
            //return RedirectToAction("/home");
        }

        public IActionResult LoginForm(string returnUrl = null, string error = null)
        {
            ViewBag.Error = error;
            ViewBag.ReturnUrl = returnUrl;
            if (string.IsNullOrEmpty(returnUrl))
            {
                Uri uri = new Uri(Request.GetTypedHeaders().Referer.ToString());
                ViewBag.ReturnUrl = uri.LocalPath;
            }

            return View();
        }
    }
}
