﻿using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Logging;
using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Diagnostics;
using System.Linq;
using System.Threading.Tasks;
using WebApplication3.Models;
using SQLaccess;
using Email;
using Microsoft.Extensions.Options;
using System.Threading;
using static OracleAccess.OracleDataAccess;
using Oracle.ManagedDataAccess.Client;
using Encrypt;
using SignalRChat.Hubs;
using Microsoft.AspNetCore.SignalR;
using ServiceReference2;
using DataCollectionReportingSystem.Classes;
using Newtonsoft.Json;


namespace WebApplication3.Controllers
{
    public class HomeController : Controller
    {
        private readonly ILogger<HomeController> _logger;
        private readonly ISQLaccess sqlService;
        //private readonly IOracleaccess oracleService;
        private readonly MailSettings mailsettings;
        private readonly IEncryptionService encryptionService;
        private IHubContext<ChatHub> Hub { get; set; }

        //, IOracleaccess oracleService
        public HomeController(ILogger<HomeController> logger, ISQLaccess sqlService, IOptions<MailSettings> mailsettings, IEncryptionService encryptionService, IHubContext<ChatHub> hub)//
        {
            _logger = logger;
            this.sqlService = sqlService;
            //this.oracleService = oracleService;
            this.mailsettings = mailsettings.Value;
            this.encryptionService = encryptionService;
            Hub = hub;
        }
        [Authorize(Roles = "User")]
        public IActionResult Index()
        {
            if (User != null && User.Identity.IsAuthenticated)
            {
                string[] userInfo = User.Identity.Name.Split('|');
            }

            dcrs_SQL dcrs_SQL = new dcrs_SQL();
            DataCollectionReportingSystemDTO DataCollectionReportingSystemDTO = new DataCollectionReportingSystemDTO();
            List<FirstPass> FirstPassList = dcrs_SQL.FPY(DateTime.Now.ToShortDateString() + " 12:00:00 AM", DateTime.Now.ToShortDateString() + " 11:59:59 PM");
            DataCollectionReportingSystemDTO.fpy = FirstPassList;
            DataCollectionReportingSystemDTO.fpy = DataCollectionReportingSystemDTO.fpy.OrderByDescending(x => x.partNum).ToList();
            DataCollectionReportingSystemDTO.failures = new List<Failure>();
            DataCollectionReportingSystemDTO.cpk = new List<Cpk_calculation>();
            DataCollectionReportingSystemDTO.fpySort = "partNum;desc;string";
            DataCollectionReportingSystemDTO.failuresSort = "partNum;desc;string";
            ViewBag.DataCollection = JsonConvert.SerializeObject(DataCollectionReportingSystemDTO);
            return View(DataCollectionReportingSystemDTO);
        }

        //[Authorize]
        //[Authorize(Roles = "User")]
        //public async Task<IActionResult> PrivacyAsync()
        //{
        //    //List<CustomerModel> custList = new List<CustomerModel>();
        //    ServiceReference2.CustomerFilter customerFilter = new ServiceReference2.CustomerFilter();
        //    customerFilter.Filter = new ServiceReference2.GridFilter();
        //    customerFilter.Search = new ServiceReference2.CustomerSearch();
        //    ServiceReference2.Service1Client service1Client = new ServiceReference2.Service1Client();
        //    var custList = await service1Client.GetCustomerListAsync(customerFilter);
        //    string sdf = custList[0].Address;
        //    service1Client.Close();

        //    wsServiceReference1.ServiceClient client = new wsServiceReference1.ServiceClient();
        //    //client.ClientCredentials.Windows.ClientCredential.Domain = "HME";
        //    //client.ClientCredentials.UserName.UserName = "ax-bcproxy";
        //    //client.ClientCredentials.UserName.Password = "H,wn3Vsg";
        //    //client.ClientCredentials.ServiceCertificate.Authentication.CertificateValidationMode = System.ServiceModel.Security.X509CertificateValidationMode.PeerOrChainTrust;

        //    var rrrr = await client.GetDataAsync(1);
        //    wsServiceReference1.CompositeType fffff = new wsServiceReference1.CompositeType();
        //    fffff.BoolValue = true;
        //    var ttttt = await client.GetDataUsingDataContractAsync(fffff);
        //    client.Close();

        //    if (User != null && User.Identity.IsAuthenticated)
        //    {
        //        string[] userInfo = User.Identity.Name.Split('|');

        //    }
        //    //var xcurrency = cache.RuntimeCaching.CacheGet("currency");
        //    cache.RuntimeCaching.CacheSave("currency" , "ffffff");
        //    var currency = cache.RuntimeCaching.CacheGet("currency");

        //    string x = encryptionService.Encrypt("ddddddd", true);
        //    string xx = encryptionService.Encrypt("ddddddd", false);
        //    //sqlService.DBSQLsetup("data source=DCAXDEVSQL02;initial catalog=AX12R3TEST1;Integrated Security=True;");
        //    sqlService.DBSQLsetup("data source=DCAXDEVSQL02;user id=DB_WEB_USER;pwd=lx3O64kC/L587}W5;initial catalog=AX12R3TEST1;");
        //    DataSet ds;
        //    List<SqlParameter> sp = new List<SqlParameter>();
        //    string sqlMessage = string.Empty;
        //    sp.Add(new SqlParameter("hmesaleschannel", "2"));
        //    sp.Add(new SqlParameter("dataareaid", "hsc"));
        //    //sp.Add(new SqlParameter("custgroup", brand));
        //    ds = sqlService.executeStoredProcedure("sp_GetBrands", sp, out sqlMessage);
        //    sqlService.Dispose();
        //    ds.Dispose();

        //    List<OracleParameter> _sp = new List<OracleParameter>();
        //    oracleService.DBOraclesetup("Data Source=10.10.172.116:1521/agile9;User ID=agilep;Password=tartanhmeprod;");
        //    _sp.Add(new OracleParameter("IT_NO", "G29696-3"));
        //    _sp.Add(new OracleParameter("RV_N", "A"));
        //    OracleParameter cursor = new OracleParameter();
        //    cursor.OracleDbType = OracleDbType.RefCursor;
        //    cursor.Direction = ParameterDirection.Output;
        //    cursor.ParameterName = "tbl";
        //    _sp.Add(cursor);
        //    ds = oracleService.executeStoredProcedure("AssemblyAttachments", _sp, out sqlMessage);
        //    oracleService.Dispose();
        //    ds.Dispose();


        //    email email = new email(mailsettings);//mailsettings
        //    email.emailaddress emailaddress;
        //    //email.attachment attachment;
        //    email.from = "no-reply@hme.com";
        //    email.subject = "subject";
        //    email.to = new List<email.emailaddress>();
        //    emailaddress = new email.emailaddress();
        //    email.to.Add(emailaddress);
        //    emailaddress.address = "hscott@hme.com";
        //    EmailThread EmailThread = new EmailThread();
        //    Thread emailThread = new Thread(EmailThread.sendEmail);
        //    emailThread.Start(email);

        //    //bool ret = false;
        //    //int retryCount = 0;
        //    //while (!ret && retryCount < 5)
        //    //{
        //    //    ret = email.sendemail();
        //    //    if (!ret) { Thread.Sleep(2000); retryCount++; }
        //    //}
        //    email.Dispose();
        //    return View();
        //}

        [ResponseCache(Duration = 0, Location = ResponseCacheLocation.None, NoStore = true)]
        public IActionResult Error()
        {
            return View(new ErrorViewModel { RequestId = Activity.Current?.Id ?? HttpContext.TraceIdentifier });
        }

        [Authorize(Roles = "User")]
        [HttpGet]
        public JsonResult FPY(string firstPassStartDate, string firstPassEndDate, string partnumber = null)
        {

            dcrs_SQL dcrs_SQL = new dcrs_SQL();
            DataCollectionReportingSystemDTO DataCollectionReportingSystemDTO = new DataCollectionReportingSystemDTO();

            DataCollectionReportingSystemDTO.fpy = dcrs_SQL.FPY(firstPassStartDate, firstPassEndDate, partnumber);
           // DataCollectionReportingSystemDTO.fpy = DataCollectionReportingSystemDTO.fpy.OrderByDescending(x => x.partNum).ToList();
            DataCollectionReportingSystemDTO.status = "OK";
            //string s = JsonConvert.SerializeObject(DataCollectionReportingSystemDTO);
            return Json(DataCollectionReportingSystemDTO);
        }

        [Authorize(Roles = "User")]
        [HttpGet]
        public JsonResult Failures(string firstPassStartDate, string firstPassEndDate, string partnumber = null)
        {

            dcrs_SQL dcrs_SQL = new dcrs_SQL();
            DataCollectionReportingSystemDTO DataCollectionReportingSystemDTO = new DataCollectionReportingSystemDTO();

            DataCollectionReportingSystemDTO.failures = dcrs_SQL.Failures(firstPassStartDate, firstPassEndDate, partnumber);
            DataCollectionReportingSystemDTO.status = "OK";
            //string s = JsonConvert.SerializeObject(DataCollectionReportingSystemDTO);
            return Json(DataCollectionReportingSystemDTO);
        }

        [Authorize(Roles = "User")]
        [HttpGet]
        public JsonResult Cpk(string firstPassStartDate, string firstPassEndDate, string partnumber = null)
        {
            dcrs_SQL dcrs_SQL = new dcrs_SQL();
            DataCollectionReportingSystemDTO DataCollectionReportingSystemDTO = new DataCollectionReportingSystemDTO();
            DataCollectionReportingSystemDTO.cpk = dcrs_SQL.Cpk_calculation(firstPassStartDate, firstPassEndDate, partnumber);
            DataCollectionReportingSystemDTO.status = "OK";
            return Json(DataCollectionReportingSystemDTO);
        }

        [Authorize(Roles = "User")]
        [HttpPost]
        public JsonResult Test([FromBody] FPY_DTO dto)
        {          
            return Json("test");
        }
    }
    public class EmailThread
    {
        public void sendEmail(object emailObj)
        {
            email mail = (email)emailObj;
            bool ret = false;
            int retryCount = 0;
            while (!ret && retryCount < 5)
            {
                ret = mail.sendemail();
                if (!ret) { Thread.Sleep(2000); retryCount++; }
            }
        }
    }
}