﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Threading.Tasks;
using SQLaccess;

namespace WebApplication3
{
    public class LdapConfig
    {
        public string Path { get; set; }
        public string UserDomainName { get; set; }
    }
    public interface IAuthenticationService
    {
        User Login(string userName, string password,string returnUrl);
        //void Loginout();

    }
    public class User
    {
        public string UserName { get; set; }
        public string DisplayName { get; set; }
        // other properties
    }
    public class LoginViewModel
    {
        [Required]
        [Display(Name = "Email")]
        [EmailAddress]
        public string Username { get; set; }

        [Required]
        [DataType(DataType.Password)]
        [StringLength(100, ErrorMessage = "The {0} must be at least {2} characters long.", MinimumLength = 6)]
        [Display(Name = "Password")]
        public string Password { get; set; }

        [Display(Name = "Remember me?")]
        public bool RememberMe { get; set; }
    }

    //public interface ISQLaccess
    //{
    //    NonQueryMessage execNonQueryString(string sqlString);
    //    void DBSQsetup(string connString);
    //    void Dispose();
    //    DataSet executeStoredProcedure(string storedProcedureName, List<SqlParameter> sqlparams, out string message);
    //}

    //class MyConsoleLogger : ILog
    //{
    //    public void info(string str)
    //    {
    //        Console.WriteLine(str);
    //    }
    //}
}
