﻿using System;
using System.IO;
using Microsoft.Extensions.Configuration;

namespace Custom
{
    public class CustomAppSettings
    {
        public static class ConfigurationManager
        {
            public static IConfiguration AppSetting { get; }
            static ConfigurationManager()
            {
                AppSetting = new ConfigurationBuilder()
                        .SetBasePath(Directory.GetCurrentDirectory())
                        .AddJsonFile("CustomAppSettings.json")
                        .Build();
            }
        }
    }
}
