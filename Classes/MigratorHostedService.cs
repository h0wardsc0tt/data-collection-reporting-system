﻿using ClearCom.Classes;
using Microsoft.AspNetCore.SignalR;
using Microsoft.Extensions.Hosting;
using SignalRChat.Hubs;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading;
using System.Threading.Tasks;

namespace ClearCom
{

    public class MigratorHostedService : IHostedService
    {
        public static  RecordChangedEvent_Cell recordChangedEvent_Cell;
        private IHubContext<ChatHub> Hub { get; set; }
        public MigratorHostedService(IHubContext<ChatHub> hub)
        {
            Hub = hub;
        }
        public async Task StartAsync(CancellationToken cancellationToken)
        {
            recordChangedEvent_Cell = new RecordChangedEvent_Cell();
            //await recordChangedEvent_Cell.MonitorCells(Hub);

            ////throw new NotImplementedException();
        }

        public Task StopAsync(CancellationToken cancellationToken)
        {
            //recordChangedEvent_Cell.Dispose();
            return Task.CompletedTask;

        }
    }
}
