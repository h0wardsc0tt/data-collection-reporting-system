﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Reflection;
using SQLaccess;


namespace DataCollectionReportingSystem.Classes
{
    public class dcrs_SQL
    {
        public List<FirstPass> FPY(string firstPassStartDate, string firstPassEndDate, string partnumber = null)
        {
            dbSQL sqlService = new dbSQL();
            //sqlService.DBSQLsetup("data source=powsql;user id=DataCollection;pwd=DataCollection;initial catalog=ProdTest;");
            //sqlService.DBSQLsetup("data source=powsql02;user id=DataCollection;pwd=D@taC0llection!;initial catalog=ProdTest_Repl;");
            sqlService.DBSQLsetup("data source=powsql02;user id=DataCollection;pwd=Data&Collection2022!;initial catalog=ProdTest_Repl;");//D@taC0llection!
            DataSet ds;
            List<SqlParameter> sp = new List<SqlParameter>();
            string sqlMessage = string.Empty;
            sp.Add(new SqlParameter("start", firstPassStartDate));
            sp.Add(new SqlParameter("end", firstPassEndDate));

            //sp.Add(new SqlParameter("StartDate", "8/31/2021 12:00:00 AM"));
            //sp.Add(new SqlParameter("EndDate", "8/31/2021 11:59:59 PM"));
            if (partnumber != null)
            {
                sp.Add(new SqlParameter("PN", partnumber));
                //sp.Add(new SqlParameter("PN", "T13880-1Z1"));
            }
            ds = sqlService.executeStoredProcedure("sp_DEV_Web_getFPY", sp, out sqlMessage);
            sqlService.Dispose();
            IList<PropertyInfo> propertiesList;
            propertiesList = typeof(FirstPass).GetProperties().ToList();
            FirstPass firstPass = new FirstPass();
            List<FirstPass> fpyList = new List<FirstPass>();
            int count = 0;
            foreach (DataRow row in ds.Tables[0].Rows)
            {
                ////if (row["PartNum"].ToString() != "G29797-1TD1xx") {
                //    if (count <= 227)
                //    {
                firstPass = new FirstPass();
                foreach (var property in propertiesList)
                {
                    switch (property.Name)
                    {
                        case "partDesc":
                            firstPass.SetProperty(property.Name, row[property.Name].ToString().Trim());
                            break;
                        case "passQty":
                        case "failQty":
                        case "totalQty":
                            firstPass.SetProperty(property.Name, (int)row[property.Name]);
                            break;
                        default:
                            firstPass.SetProperty(property.Name, row[property.Name].ToString().Trim());
                            break;
                    }
                }
                fpyList.Add(firstPass);
                //    }
                //    count++;
                //}

            }
            ds.Dispose();
            return fpyList;
        }

        public List<Failure> Failures(string firstPassStartDate, string firstPassEndDate, string partnumber = null)
        {
            dbSQL sqlService = new dbSQL();
            //sqlService.DBSQLsetup("data source=powsql;user id=DataCollection;pwd=DataCollection;initial catalog=ProdTest;");
            //sqlService.DBSQLsetup("data source=powsql02;user id=DataCollection;pwd=D@taC0llection!;initial catalog=ProdTest_Repl;");

            sqlService.DBSQLsetup("data source=powsql02;user id=DataCollection;pwd=Data&Collection2022!;initial catalog=ProdTest_Repl;");//D@taC0llection!
            DataSet ds;
            List<SqlParameter> sp = new List<SqlParameter>();
            string sqlMessage = string.Empty;
            sp.Add(new SqlParameter("start", firstPassStartDate));
            sp.Add(new SqlParameter("end", firstPassEndDate));

            //sp.Add(new SqlParameter("StartDate", "8/31/2021 12:00:00 AM"));
            //sp.Add(new SqlParameter("EndDate", "8/31/2021 11:59:59 PM"));
            if (partnumber != null)
            {
                sp.Add(new SqlParameter("PN", partnumber));
                //sp.Add(new SqlParameter("PN", "T13880-1Z1"));
            }
            ds = sqlService.executeStoredProcedure("sp_DEV_Web_getStepFailures", sp, out sqlMessage);
            sqlService.Dispose();
            IList<PropertyInfo> propertiesList;
            propertiesList = typeof(Failure).GetProperties().ToList();
            Failure procedureData = new Failure();
            List<Failure> failuresList = new List<Failure>();
            foreach (DataRow row in ds.Tables[0].Rows)
            {
                procedureData = new Failure();
                foreach (var property in propertiesList)
                {
                    switch (property.Name)
                    {

                        case "xTestStep":
                            procedureData.SetProperty(property.Name, (int)row[property.Name]);
                            break;
                        case "xTestDate":
                            procedureData.SetProperty(property.Name, ((DateTime)row[property.Name]).ToShortDateString());
                            break;
                        default:
                            procedureData.SetProperty(property.Name, row[property.Name].ToString().Trim());
                            break;
                    }
                }
                failuresList.Add(procedureData);
            }
            ds.Dispose();
            return failuresList;
        }

        public List<Cpk_calculation> Cpk_calculation(string firstPassStartDate, string firstPassEndDate, string partnumber = null)
        {
            dbSQL sqlService = new dbSQL();
            //sqlService.DBSQLsetup("data source=powsql;user id=DataCollection;pwd=DataCollection;initial catalog=ProdTest;");
            sqlService.DBSQLsetup("data source=powsql02;user id=DataCollection;pwd=Data&Collection2022!;initial catalog=ProdTest_Repl;");//D@taC0llection!
            DataSet ds;
            List<SqlParameter> sp = new List<SqlParameter>();
            string sqlMessage = string.Empty;
            sp.Add(new SqlParameter("start", firstPassStartDate));
            sp.Add(new SqlParameter("end", firstPassEndDate));
            if (partnumber != "null" && partnumber != null)
            {
                sp.Add(new SqlParameter("PN", partnumber));//"G30070-1A1"
            }
            ds = sqlService.executeStoredProcedure("[sp_DEV_Web_getCPK]", sp, out sqlMessage);
            sqlService.Dispose();
            IList<PropertyInfo> propertiesList;
            propertiesList = typeof(Cpk_calculation).GetProperties().ToList();
            Cpk_calculation cpk_calculation = new Cpk_calculation();
            List<Cpk_calculation> cpk_calculationList = new List<Cpk_calculation>();
            foreach (DataRow row in ds.Tables[0].Rows)
            {
                cpk_calculation = new Cpk_calculation();
                foreach (var property in propertiesList)
                {
                    switch (property.Name)
                    {
                        case "TestStep":
                            cpk_calculation.SetProperty(property.Name, (int)row[property.Name]);
                            break;
                        case "CpL":
                        case "CpU":
                        case "UL":
                        case "LL":
                        case "Average":
                        case "StdDev":
                        case "SD3":
                        case "Cpk":
                            var xx = row[property.Name];
                            if (row[property.Name] != DBNull.Value)
                                cpk_calculation.SetProperty(property.Name, (double)row[property.Name]);
                            break;
                        default:
                            cpk_calculation.SetProperty(property.Name, row[property.Name].ToString().Trim());
                            break;
                    }
                }
                cpk_calculationList.Add(cpk_calculation);
            }
            ds.Dispose();
            return cpk_calculationList;
        }
    }


    public class DataCollectionReportingSystemDTO
    {
        public List<FirstPass> fpy { get; set; }
        public List<Cpk_calculation> cpk { get; set; }
        public List<Failure> failures { get; set; }
        public string status { get; set; }
        public string message { get; set; }
        public string fpySort { get; set; }
        public string cpkSort { get; set; }
        public string failuresSort { get; set; }
    }


    public class FirstPass
    {
        public string testType { get; set; }
        public string partNum { get; set; }
        public string partRev { get; set; }
        public string partDesc { get; set; }
        public int passQty { get; set; }
        public int failQty { get; set; }
        public int totalQty { get; set; }
        public string fpyate { get; set; }
    }


    public class Failure
    {
        public string TestType { get; set; }
        public string PartNum { get; set; }
        public string PartRev { get; set; }
        public string SerialNum { get; set; }
        public string TestStep { get; set; }
        public string TestName { get; set; }
        public string Pass_Fail { get; set; }
        public string LowerLimit { get; set; }
        public string UpperLimit { get; set; }
        public string MeasValue { get; set; }
        public string Units { get; set; }
        public string TestDate { get; set; }
    }

    public class Cpk_calculation
    {
        public string PartNum { get; set; }
        public int TestStep { get; set; }
        public string TestName { get; set; }
        public double LL { get; set; }
        public double UL { get; set; }
        public double CpL { get; set; }
        public double CpU { get; set; }
        public double Average { get; set; }
        public double StdDev { get; set; }
        public double SD3 { get; set; }
        public double Cpk { get; set; }
        public string Units { get; set; }

    }

    public class FPY_DTO
    {
        public string firstPassStartDate { get; set; }
        public string firstPassEndDate { get; set; }
    }

    public static class Extensions
    {
        public static void SetProperty(this object obj, string propertyName, object value)
        {
            var propertyInfo = obj.GetType().GetProperty(propertyName);
            if (propertyInfo == null) return;
            propertyInfo.SetValue(obj, value);
        }
    }

}
