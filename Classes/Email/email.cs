﻿using Microsoft.Win32.SafeHandles;
using System;
using System.Collections.Generic;
using System.IO;
using System.Net.Mail;
using System.Net.Mime;
using System.Runtime.InteropServices;

namespace Email
{
    public class email : IDisposable
    {
        private readonly MailSettings mailsettings;

        public email(MailSettings mailsettings)
        {
            this.mailsettings = mailsettings;
        }
        public bool sendemail()
        {
            string mes;
            if (to == null || subject == string.Empty || body == string.Empty || from == string.Empty)
                return false;
            try
            {
                MemoryStream memoryStream = null;
                Attachment attachment;
                MailMessage mail = new MailMessage()
                {
                    IsBodyHtml = true,
                    Subject = subject,
                    Body = body,
                    From = new MailAddress(from)
                };
                foreach (emailaddress addr in to)
                {
                    mail.To.Add(addr.address);
                }
                if (cc != null)
                {
                    foreach (emailaddress addr in cc)
                    {
                        mail.CC.Add(addr.address);
                    }
                }
                if (bcc != null)
                {
                    foreach (emailaddress addr in bcc)
                    {
                        mail.Bcc.Add(addr.address);
                    }
                }

                if (attachments != null)
                {
                    foreach (attachment att in attachments)
                    {
                        if (att.inline)
                        {
                            switch (att.type)
                            {
                                case AttachmentType.byteArray:
                                    memoryStream = new MemoryStream(att.bytearray);
                                    attachment = new Attachment(memoryStream, att.filename, getContentType(Path.GetExtension(att.filename)));
                                    attachment.ContentId = att.inlineid;
                                    attachment.ContentDisposition.Inline = true;
                                    attachment.ContentDisposition.DispositionType = DispositionTypeNames.Inline;
                                    mail.Attachments.Add(attachment);
                                    break;
                                default:
                                    attachment = new Attachment(att.filepath);
                                    attachment.ContentId = att.inlineid;// new Random().Next(100000, 9999999).ToString();
                                    attachment.ContentDisposition.Inline = true;
                                    attachment.ContentDisposition.DispositionType = DispositionTypeNames.Inline;
                                    mail.Attachments.Add(attachment);
                                    break;
                            }
                        }
                        else
                        {
                            switch (att.type)
                            {
                                case AttachmentType.byteArrayPDF:
                                    memoryStream = new MemoryStream(att.bytearray);
                                    attachment = new Attachment(memoryStream, att.filename, MediaTypeNames.Application.Pdf);
                                    mail.Attachments.Add(attachment);
                                    break;
                                case AttachmentType.byteArrayxlsx:
                                    memoryStream = new MemoryStream(att.bytearray);
                                    attachment = new Attachment(memoryStream, att.filename, "application/vnd.openxmlformats-officedocument.spreadsheetml.sheet");
                                    mail.Attachments.Add(attachment);
                                    break;
                                case AttachmentType.byteArray:
                                    memoryStream = new MemoryStream(att.bytearray);
                                    attachment = new Attachment(memoryStream, att.filename, getContentType(Path.GetExtension(att.filename)));
                                    mail.Attachments.Add(attachment);
                                    break;
                                default:
                                    attachment = new Attachment(att.filepath);
                                    mail.Attachments.Add(attachment);
                                    break;
                            }
                        }
                    }
                }

                SmtpClient client = new SmtpClient(mailsettings.Host, mailsettings.Port);
                try
                {
                    client.Send(mail);
                }
                catch (Exception e)
                {
                    mes = e.Message;
                    return false;
                }
                return true;
            }
            catch (Exception ex) { string msg = ex.Message; return false; }
        }

        readonly SafeHandle handle = new SafeFileHandle(IntPtr.Zero, true);
        bool disposed = false;
        public string from { get; set; }
        public string subject { get; set; }
        public string body { get; set; }
        public List<emailaddress> to { get; set; }
        public List<emailaddress> cc { get; set; }
        public List<emailaddress> bcc { get; set; }
        public List<attachment> attachments { get; set; }
        public class emailaddress
        {
            public string address { get; set; }
        }
        public class attachment
        {
            public string filename { get; set; }
            public string filepath { get; set; }
            public byte[] bytearray { get; set; }
            public MemoryStream memoryStream { get; set; }
            public AttachmentType type { get; set; }
            public string inlineid { get; set; }
            public bool inline { get; set; }
        }
        public enum AttachmentType
        {
            filePath,
            byteArray,
            byteArrayPDF,
            memoryStream,
            byteArrayxlsx,
        }
        private string getContentType(string prefix)
        {
            string content_type = string.Empty;
            switch (prefix)
            {
                case ".pdf":
                    content_type = "application/pdf";
                    break;
                case ".jpg":
                    content_type = "image/jpg";
                    break;
                case ".tif":
                    content_type = "image/tif; cht=iso-8859-1";
                    break;
                case ".xlsx":
                    content_type = "application/vnd.openxmlformats-officedocument.spreadsheetml.sheet";
                    break;
                case ".xls":
                    content_type = "application/vnd.ms-excel";
                    break;
                case ".xlsm":
                    content_type = "application/vnd.ms-excel.sheet.macroEnabled.12";
                    break;
                case "xla":
                    content_type = "application/vnd.ms-excel";
                    break;
                case ".xltx":
                    content_type = "application/vnd.openxmlformats-officedocument.spreadsheetml.template";
                    break;
                case ".doc":
                    content_type = "application/msword";
                    break;
                case ".docx":
                    content_type = "application/vnd.openxmlformats-officedocument.wordprocessingml.document";
                    break;
                case ".msg":
                    content_type = "application/vnd.ms-outlook";
                    break;
                case ".mht":
                    content_type = "application/octet-stream";
                    break;
                case ".rtf":
                    content_type = "application/msword";
                    break;
                case ".txt":
                    content_type = "text/plain";
                    break;
                case ".gif":
                    content_type = "image/gif";
                    break;
                default:
                    content_type = "application/octet-stream";
                    break;
            }
            return content_type;
        }
        public void Dispose()
        {
            Dispose(true);
            GC.SuppressFinalize(this);
        }
        protected virtual void Dispose(bool disposing)
        {
            try
            {
                if (disposed)
                    return;

                if (disposing)
                {
                    handle.Dispose();
                    // Free any other managed objects here.
                }

                // Free any unmanaged objects here.
                //
                disposed = true;
            }
            catch { }
        }
    }
}
