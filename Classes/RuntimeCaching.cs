﻿using System;
using System.Runtime.Caching;

namespace cache
{
    public class RuntimeCaching
    {
        public static int CacheLoginAttempts(string ip)
        {
            ObjectCache cache = MemoryCache.Default;
            CacheItemPolicy cacheItemPolicy = new CacheItemPolicy();
            int count = 0;
            if (cache[ip] != null)
            {
                count = (int)cache.Get(ip);
                cache.Remove(ip);
            }
            int newcount = count + 1;
            if (newcount < 5)
            {
                cacheItemPolicy.AbsoluteExpiration = DateTime.Now.AddSeconds(30);
                ClearCache(ip);
                cache.Add(ip, newcount, cacheItemPolicy);
                //cache.Insert(ip, newcount, null, DateTime.Now.AddSeconds(30), System.Web.Caching.Cache.NoSlidingExpiration);
            }
            else
            {
                newcount = 99;
                cacheItemPolicy.AbsoluteExpiration = DateTime.Now.AddMinutes(2);
                ClearCache(ip);
                cache.Add(ip + "delay", newcount, cacheItemPolicy);
                //cache.Insert(ip + "delay", newcount, null, DateTime.Now.AddMinutes(2), System.Web.Caching.Cache.NoSlidingExpiration);

            }
            return newcount;
        }
        public static int GetLoginAttempts(string ip)
        {
            ObjectCache cache = MemoryCache.Default;
            if (cache[ip] != null)
            {
                return (int)cache.Get(ip);
            }
            else
                return 0;
        }

        public static void ClearCache(string handle)
        {
            ObjectCache cache = MemoryCache.Default;
            if (cache[handle] != null)
            {
                cache.Remove(handle);
            }
        }
        public static void CacheForgotPassword(string uid)
        {
            ObjectCache cache = MemoryCache.Default;
            CacheItemPolicy cacheItemPolicy = new CacheItemPolicy();
            if (cache[uid] != null)
            {
                cache.Remove(uid);
            }
            cacheItemPolicy.AbsoluteExpiration = DateTime.Now.AddMinutes(120);
            cache.Add(uid, "0", cacheItemPolicy);
            //cache.Insert(uid, 0, null, DateTime.Now.AddMinutes(120), System.Web.Caching.Cache.NoSlidingExpiration);
        }
        public static bool CacheIsForgotPassword(string uid)
        {
            ObjectCache cache = MemoryCache.Default;
            if (cache[uid] != null)
            {
                cache.Remove(uid);
                return true;
            }
            return false;
        }
        public static void CacheSave(string id, string value, double? duration = null)
        {
            ObjectCache cache = MemoryCache.Default;
            CacheItemPolicy cacheItemPolicy = new CacheItemPolicy();
            if (cache[id] != null)
            {
                cache.Remove(id);
            }
            cacheItemPolicy.AbsoluteExpiration = duration == null ? DateTime.Now.AddMinutes(20) : DateTime.Now.AddMinutes((double)duration);
            cache.Add(id, value, cacheItemPolicy);
        }
        public static string CacheGet(string id)
        {
            ObjectCache cache = MemoryCache.Default;
            string currency = string.Empty;
            if (cache[id] != null)
            {
                currency = cache.Get(id).ToString();
                cache.Remove(id);
                return currency;
            }
            return null;
        }
    }
}