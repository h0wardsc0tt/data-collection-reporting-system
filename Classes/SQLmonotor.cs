﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using System.Configuration;
using System.IO;
using Newtonsoft.Json;
using TableDependency.SqlClient;
using TableDependency.SqlClient.Base;
using System.Threading;
using TableDependency.SqlClient.Base.EventArgs;
using TableDependency.SqlClient.Base.Enums;
using ErrorEventArgs = TableDependency.SqlClient.Base.EventArgs.ErrorEventArgs;
using Microsoft.Extensions.Configuration;
using Microsoft.AspNetCore.SignalR;
using SignalRChat.Hubs;

namespace ClearCom.Classes
{
    public class RecordChangedEvent_Cell
    {
        public SqlTableDependency<pfdbrow> table_row;
        private IHubContext<ChatHub> Hub;
        bool connectionComplete = false;
        ModelToTableMapper<pfdbrow> mapper = new ModelToTableMapper<pfdbrow>();
        readonly string x1 = @"data source = DCPRDAXSQL02; user id = WEB_USER_DBMONITOR; pwd = lx3O64kC/L587}W5; initial catalog = AXProdFloorChart;";
        readonly string x2 = "tblRtAllDaysCells3";

        public async Task MonitorCells(IHubContext<ChatHub> hub)//IHubContext<ChatHub> hub
        {
            Hub = hub;
            mapper.AddMapping(s => s.key, "key");
            await StartConnectionAsync();

        }

        private async Task StartConnectionAsync()
        {
            return;
            connectionComplete = false;
            while (!connectionComplete)
            {
                try
                {
                    //string myDb1ConnectionString = Configuration.GetConnectionString("myDb1");
                    //List<string> updateOf = new List<string>()
                    //{
                    //    //"New York",
                    //    //"London",
                    //    //"Mumbai",
                    //    "Holdings1"
                    //};

                    await Task.Run(() => { table_row = new SqlTableDependency<pfdbrow>(x1, x2, "dbo", mapper); });//, updateOf
                    connectionComplete = true;
                }
                catch (Exception er)
                {
                    Thread.Sleep(5000);
                }
            }
            table_row.OnChanged += SqlTable_Changed;
            table_row.OnError += SqlTable_OnError;
            try
            {
                await Task.Run(() => { table_row.Start(); });
            }
            catch
            {};
        }
        private async void SqlTable_Changed(object sender, RecordChangedEventArgs<pfdbrow> e)
        {
            if (e.ChangeType != ChangeType.None)
            {
                dbMonitor_dto dto = new dbMonitor_dto();
                dto.table = ConfigurationManager.AppSettings["Monitor_Table"];
                dto.updatetype = e.ChangeType.ToString();
                dto.tblupdate = e.Entity;
                dto.tblupdate.Date1 = DateTime.Parse(dto.tblupdate.Date1.ToString()).ToShortDateString();
                dto.tblupdate.Date2 = DateTime.Parse(dto.tblupdate.Date2.ToString()).ToShortDateString();
                dto.tblupdate.Date3 = DateTime.Parse(dto.tblupdate.Date3.ToString()).ToShortDateString();
                dto.tblupdate.Date4 = DateTime.Parse(dto.tblupdate.Date4.ToString()).ToShortDateString();
                dto.tblupdate.Date5 = DateTime.Parse(dto.tblupdate.Date5.ToString()).ToShortDateString();
                dto.tblupdate.Short_Amount = dto.tblupdate.Short_Amount == null ? "" : dto.tblupdate.Short_Amount;

                string jsonDTO = JsonConvert.SerializeObject(dto);

                try
                {
                    await Hub.Clients.All.SendAsync("dbBroadcast", "db", "table", jsonDTO);
                }
                catch { }
            }
            else
            {
                Thread.Sleep(2000);
                await StartConnectionAsync();
            }
        }

        private async void SqlTable_OnError(object sender, ErrorEventArgs e)
        {
            Thread.Sleep(5000);
            await StartConnectionAsync();
        }

        private void SqlTable_Status_Change(object sender, StatusChangedEventArgs e)
        {
            var status = e.Status;
        }

        private bool disposedValue = false; // To detect redundant calls

        protected virtual void Dispose(bool disposing)
        {
            if (!disposedValue)
            {
                if (disposing)
                {
                    try
                    {
                        table_row.Stop();
                        disposedValue = true;
                    }
                    catch
                    {
                        disposedValue = false;
                    }
                }

            }
        }

        ~RecordChangedEvent_Cell()
        {
            Dispose(false);
        }

        // This code added to correctly implement the disposable pattern.
        public void Dispose()
        {
            Dispose(true);
            GC.SuppressFinalize(this);
        }

        public class dbMonitor_dto
        {
            public pfdbrow tblupdate { get; set; }
            public string updatetype { get; set; }
            public string table { get; set; }
        }

        [Serializable]
        public class pfdbrow
        {
            public string key { get; set; }
            public int Cell { get; set; }
            public string PN { get; set; }
            public string PartDesc { get; set; }
            public string Date1 { get; set; }
            public string Day1 { get; set; }
            public float Bal1 { get; set; }
            public float Bookings1 { get; set; }
            public float Holdings1 { get; set; }
            public string Date2 { get; set; }
            public string Day2 { get; set; }
            public float Bal2 { get; set; }
            public float Bookings2 { get; set; }
            public float Holdings2 { get; set; }
            public string Date3 { get; set; }
            public string Day3 { get; set; }
            public float Bal3 { get; set; }
            public float Bookings3 { get; set; }
            public float Holdings3 { get; set; }
            public string Date4 { get; set; }
            public string Day4 { get; set; }
            public float Bal4 { get; set; }
            public float Bookings4 { get; set; }
            public float Holdings4 { get; set; }
            public string Date5 { get; set; }
            public string Day5 { get; set; }
            public float Bal5 { get; set; }
            public float Bookings5 { get; set; }
            public float Holdings5 { get; set; }
            public float Target { get; set; }
            public float Daily { get; set; }
            public float Moved { get; set; }
            public float NewQty { get; set; }
            public float Demand { get; set; }
            public string Short_Amount { get; set; }
            public float NewQty5 { get; set; }
            public float Shipped { get; set; }
            public float Moved5Days { get; set; }
            public float NewestQty { get; set; }
            public float FGInv { get; set; }
            public float WIPInv { get; set; }
            public float Bookings14Days { get; set; }
            public string Company { get; set; }
        }
    }

}
