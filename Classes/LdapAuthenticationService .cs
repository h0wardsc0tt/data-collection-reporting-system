﻿
using Microsoft.AspNetCore.Authentication;
using Microsoft.Extensions.Options;
using System;
using System.Collections.Generic;
using System.DirectoryServices;
using System.Linq;
using System.Threading.Tasks;

namespace WebApplication3
{
    public class LdapAuthenticationService : IAuthenticationService
    {
        private const string DisplayNameAttribute = "DisplayName";
        private const string SAMAccountNameAttribute = "SAMAccountName";

        private readonly LdapConfig config;

        public LdapAuthenticationService(IOptions<LdapConfig> config)
        {
            this.config = config.Value;
        }
        public User Login(string userName, string password, string returnUrl)
        {
            string error = string.Empty;

            if (string.IsNullOrEmpty(password))
            {
                error += "Password must be entered";
            }
            if (string.IsNullOrEmpty(userName))
            {
                error += (error == string.Empty ? "" : ", ") + "UserName must be entered";
            }
            //if(error != string.Empty)
            //{
            //    if (string.IsNullOrEmpty(returnUrl))
            //    {
            //        Redirect("/Security/LoginForm/?returnUrl=" + returnUrl + "&error=login_error");
            //    }
            //}

            try
            {
                using (DirectoryEntry entry = new DirectoryEntry(config.Path, "hme" + "\\" + userName, password))
                {
                    using (DirectorySearcher searcher = new DirectorySearcher(entry))
                    {
                        searcher.Filter = String.Format("({0}={1})", SAMAccountNameAttribute, userName);
                        searcher.PropertiesToLoad.Add(DisplayNameAttribute);
                        searcher.PropertiesToLoad.Add(SAMAccountNameAttribute);
                        var result = searcher.FindOne();
                        if (result != null)
                        {
                            var displayName = result.Properties[DisplayNameAttribute];
                            var samAccountName = result.Properties[SAMAccountNameAttribute];

                            return new User
                            {
                                DisplayName = displayName == null || displayName.Count <= 0 ? null : displayName[0].ToString(),
                                UserName = samAccountName == null || samAccountName.Count <= 0 ? null : samAccountName[0].ToString()
                            };
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                // if we get an error, it means we have a login failure.
                // Log specific exception
            }

            return null;
        }

        private void Redirect(string localPath)
        {
            throw new NotImplementedException();
        }
    }
}
