﻿using System;
using System.Data;
using Oracle.ManagedDataAccess.Client;
using System.Collections.Generic;
using static OracleAccess.OracleDataAccess;

namespace OracleAccess
{
    public class OracleDataAccess : IOracleaccess
    {
        private OracleConnection _conn;
        private OracleConnection sqlConnection;
        private string connString;
        private List<OracleParameter> paramList;
        public OracleDataAccess() { }
        public void DBOraclesetup(string ConnectionString)
        {
            // Create the connection using the specified connection string
            _conn = new OracleConnection(ConnectionString);
            this.sqlConnection = new OracleConnection();

            this.connString = string.Empty;
            this.sqlConnection = new OracleConnection();
            this.paramList = new List<OracleParameter>();
            this.connString = ConnectionString;
            //this.openConnection();

        }
        #region " Base funcions "
        private DataSet getDataSetMessage(string message)
        {
            DataTable dataTable = new DataTable("DataTableMessage");
            DataSet dataSet = new DataSet();
            dataSet.Clear();
            DataColumn dataColumn = new DataColumn("ErrorMessage", Type.GetType("System.String"));
            dataTable.Columns.Add(dataColumn);
            DataRow dataRow = dataTable.NewRow();
            dataRow["ErrorMessage"] = message;
            dataTable.Rows.Add(dataRow);
            dataSet.Tables.Add(dataTable);
            DataSet dataSet1 = dataSet;
            return dataSet1;
        }

        public OracleDataAdapter CreateSPAdapter(string spname)
        {
            OracleDataAdapter dap = new OracleDataAdapter(spname, _conn);
            dap.SelectCommand.CommandType = CommandType.StoredProcedure;
            return dap;
        }
        public DataSet GetDataSet(OracleDataAdapter dap, string[] names)
        {
            if (names != null)
                for (int i = 0; i < names.Length; i++)
                    dap.TableMappings.Add(String.Format("Table{0}", i > 0 ? (object)i : null), names[i]);
            DataSet dst = new DataSet();
            dap.Fill(dst);
            return dst;

        }
        public DataSet GetDataSet(OracleDataAdapter dap)
        {
            return GetDataSet(dap, null);
        }
        public DataTable GetTable(OracleDataAdapter dap)
        {
            DataTable tbl = new DataTable();
            dap.Fill(tbl);
            try
            {
            }
            catch
            {
            }
            return tbl;
        }
        public DataTable RunSQL(string sqlCommand)
        {
            OracleDataAdapter dap = CreateSQLAdapter(sqlCommand);
            return GetTable(dap);
        }

        public void ExecSQL(string sqlCommand)
        {
            OracleCommand cmd = CreateSQLCommand(sqlCommand);
            ExecuteCommand(cmd);
        }
        public OracleCommand CreateSQLCommand(string command)
        {
            OracleCommand cmd = new OracleCommand(command, _conn);
            return cmd;
        }
        public OracleDataAdapter CreateSQLAdapter(string command)
        {
            OracleDataAdapter dap = new OracleDataAdapter(command, _conn);
            return dap;
        }
        public int ExecuteCommand(IDbCommand cmd)
        {
            try
            {
                _conn.Open();
                cmd.ExecuteNonQuery();
                if (cmd.Parameters.Contains("@Result"))
                    return (int)((IDataParameter)cmd.Parameters["@Result"]).Value;
                else
                    return 0;
            }
            catch
            {
                return 0;
            }
            finally
            {
                _conn.Close();
            }
        }
        public DataSet executeStoredProcedure(string storedProcedureName, List<OracleParameter> sqlparams, out string message)
        {
            _conn.Open();
            OracleCommand sqlCommand = new OracleCommand()
            {
                CommandTimeout = 300
            };

            DataSet dataSet = new DataSet();
            int count = sqlparams.Count;
            message = "OK";
            bool flag = count <= 0;
            if (!flag)
            {
                int num = 0;
                while (true)
                {
                    flag = num < count;

                    if (!flag)
                    {
                        break;
                    }

                    OracleParameter item = sqlparams[num];
                    sqlCommand.Parameters.Add(item);
                    num++;
                }

                try
                {
                    sqlCommand.CommandText = storedProcedureName;
                    sqlCommand.CommandType = CommandType.StoredProcedure;
                    sqlCommand.Connection = _conn;
                    OracleDataAdapter sqlDataAdapter = new OracleDataAdapter(sqlCommand);
                    sqlDataAdapter.Fill(dataSet);
                }
                catch (Exception exception1)
                {
                    Exception exception = exception1;
                    message = exception.Message;
                    dataSet = this.getDataSetMessage(exception.Message);
                }
            }

            DataSet dataSet1 = dataSet;
            return dataSet1;
        }
        private void openConnection()
        {
            bool state = this.sqlConnection.State != ConnectionState.Closed;
            if (!state)
            {
                this.sqlConnection.ConnectionString = this.connString;
                this.sqlConnection.Open();
            }
        }
        public void Dispose()
        {
            bool state = this.sqlConnection.State == ConnectionState.Closed;
            if (!state)
            {
                this.sqlConnection.Close();
            }
            #endregion
        }
        public interface IOracleaccess
        {
            void DBOraclesetup(string connString);
            void Dispose();
            DataSet executeStoredProcedure(string storedProcedureName, List<OracleParameter> sqlparams, out string message);
        }
    }
}