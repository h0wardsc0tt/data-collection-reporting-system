﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using System.Data;
using System.Data.SqlClient;
using WebApplication3;

namespace SQLaccess
{
    [Serializable]
    public class dbSQL: ISQLaccess
    {
        private string connString;

        private List<SqlParameter> paramList;

        private SqlConnection sqlConnection;

        public dbSQL(){}

        public void DBSQLsetup(string connString)
        {
            this.connString = string.Empty;
            this.sqlConnection = new SqlConnection();
            this.paramList = new List<SqlParameter>();
            this.connString = connString;
            this.openConnection();
        }

        public string ConnString
        {
            set
            {
                this.connString = value;
            }
        }

        public List<SqlParameter> ParamList
        {
            get
            {
                List<SqlParameter> sqlParameters = this.paramList;
                return sqlParameters;
            }
        }

        public void Dispose()
        {
            bool state = this.sqlConnection.State == ConnectionState.Closed;
            if (!state)
            {
                this.sqlConnection.Close();
            }
        }

        public NonQueryMessage execNonQuery(CommandType queryType, string queryString, List<SqlParameter> sqlParams)
        {
            SqlCommand sqlCommand = new SqlCommand();
            int count = sqlParams.Count;
            NonQueryMessage nonQueryMessage = new NonQueryMessage(-1, "No Parameters");
            bool flag = count <= 0;
            if (!flag)
            {
                int num = 0;
                while (true)
                {
                    flag = num < count;

                    if (!flag)
                    {
                        break;
                    }

                    SqlParameter item = sqlParams[num];
                    sqlCommand.Parameters.Add(item);
                    num++;
                }

                try
                {
                    this.openConnection();
                    sqlCommand.CommandText = queryString;
                    sqlCommand.CommandType = queryType;
                    sqlCommand.Connection = this.sqlConnection;
                    nonQueryMessage.RowsAffected = sqlCommand.ExecuteNonQuery();
                    nonQueryMessage.Message = string.Empty;
                }
                catch (Exception exception1)
                {
                    Exception exception = exception1;
                    nonQueryMessage.Message = exception.Message;
                    nonQueryMessage.RowsAffected = -1;
                }
            }

            NonQueryMessage nonQueryMessage1 = nonQueryMessage;
            return nonQueryMessage1;
        }

        public NonQueryMessage execNonQuery(string storedProcedureName)
        {
            SqlCommand sqlCommand = new SqlCommand();
            NonQueryMessage nonQueryMessage = new NonQueryMessage(-1, string.Empty);

            try
            {
                this.openConnection();
                sqlCommand.CommandText = storedProcedureName;
                sqlCommand.CommandType = CommandType.StoredProcedure;
                sqlCommand.Connection = this.sqlConnection;
                nonQueryMessage.RowsAffected = sqlCommand.ExecuteNonQuery();
                nonQueryMessage.Message = string.Empty;
            }
            catch (Exception exception1)
            {
                Exception exception = exception1;
                nonQueryMessage.Message = exception.Message;
                nonQueryMessage.RowsAffected = -1;
            }

            NonQueryMessage nonQueryMessage1 = nonQueryMessage;
            return nonQueryMessage1;
        }

        public NonQueryMessage execNonQueryString(string sqlString)
        {
            SqlCommand sqlCommand = new SqlCommand();
            NonQueryMessage nonQueryMessage = new NonQueryMessage(-1, string.Empty);

            try
            {
                this.openConnection();
                sqlCommand.CommandText = sqlString;
                sqlCommand.CommandType = CommandType.Text;
                sqlCommand.Connection = this.sqlConnection;
                nonQueryMessage.RowsAffected = sqlCommand.ExecuteNonQuery();
                nonQueryMessage.Message = string.Empty;
            }
            catch (Exception exception1)
            {
                Exception exception = exception1;
                nonQueryMessage.Message = exception.Message;
                nonQueryMessage.RowsAffected = -1;
            }

            NonQueryMessage nonQueryMessage1 = nonQueryMessage;
            return nonQueryMessage1;
        }

        public DataSet executeSqlString(string sqlString, List<SqlParameter> sqlparams, out string message)
        {
            this.openConnection();
            SqlCommand sqlCommand = new SqlCommand();
            DataSet dataSet = new DataSet();
            message = "OK";
            int count = sqlparams.Count;
            bool flag = count <= 0;

            if (!flag)
            {
                int num = 0;
                while (true)
                {
                    flag = num < count;

                    if (!flag)
                    {
                        break;
                    }

                    SqlParameter item = sqlparams[num];
                    sqlCommand.Parameters.Add(item);
                    num++;
                }

                try
                {
                    sqlCommand.CommandText = sqlString;
                    sqlCommand.CommandType = CommandType.Text;
                    sqlCommand.Connection = this.sqlConnection;
                    SqlDataAdapter sqlDataAdapter = new SqlDataAdapter(sqlCommand);
                    sqlDataAdapter.Fill(dataSet);
                }
                catch (Exception exception1)
                {
                    Exception exception = exception1;
                    message = exception.Message;
                    dataSet = this.getDataSetMessage(exception.Message);
                }
            }

            DataSet dataSet1 = dataSet;
            return dataSet1;
        }

        public DataSet executeSqlString(string sqlString, out string message)
        {
            this.openConnection();
            SqlCommand sqlCommand = new SqlCommand();
            DataSet dataSet = new DataSet();
            message = "OK";

            try
            {
                sqlCommand.CommandText = sqlString;
                sqlCommand.CommandType = CommandType.Text;
                sqlCommand.Connection = this.sqlConnection;
                SqlDataAdapter sqlDataAdapter = new SqlDataAdapter(sqlCommand);
                sqlDataAdapter.Fill(dataSet);
            }
            catch (Exception exception1)
            {
                Exception exception = exception1;
                message = exception.Message;
                dataSet = this.getDataSetMessage(exception.Message);
            }

            DataSet dataSet1 = dataSet;
            return dataSet1;
        }

        public DataTable executeSqlStringTable(string sqlString, out string message)
        {
            this.openConnection();
            SqlCommand sqlCommand = new SqlCommand();
            DataTable dataTable = new DataTable();
            message = "OK";

            try
            {
                sqlCommand.CommandText = sqlString;
                sqlCommand.CommandType = CommandType.Text;
                sqlCommand.Connection = this.sqlConnection;
                SqlDataAdapter sqlDataAdapter = new SqlDataAdapter(sqlCommand);
                sqlDataAdapter.Fill(dataTable);
            }
            catch (Exception exception1)
            {
                Exception exception = exception1;
                message = exception.Message;
                dataTable = this.getDataTableMessage(exception.Message);
            }

            DataTable dataTable1 = dataTable;
            return dataTable1;
        }

        public DataSet executeStoredProcedure(string storedProcedureName, List<SqlParameter> sqlparams, out string message)
        {
            this.openConnection();
            SqlCommand sqlCommand = new SqlCommand() { CommandTimeout = 300 };
            //sqlCommand.CommandTimeout = 300;
            DataSet dataSet = new DataSet();
            int count = sqlparams.Count;
            message = "OK";
            bool flag = count <= 0;
            if (!flag)
            {
                int num = 0;
                while (true)
                {
                    flag = num < count;

                    if (!flag)
                    {
                        break;
                    }

                    SqlParameter item = sqlparams[num];
                    sqlCommand.Parameters.Add(item);
                    num++;
                }

                try
                {
                    sqlCommand.CommandText = storedProcedureName;
                    sqlCommand.CommandType = CommandType.StoredProcedure;
                    sqlCommand.Connection = this.sqlConnection;
                    SqlDataAdapter sqlDataAdapter = new SqlDataAdapter(sqlCommand);
                    sqlDataAdapter.Fill(dataSet);
                }
                catch (Exception exception1)
                {
                    Exception exception = exception1;
                    message = exception.Message;
                    dataSet = this.getDataSetMessage(exception.Message);
                }
            }

            DataSet dataSet1 = dataSet;
            return dataSet1;
        }

        public DataSet executeStoredProcedure(string storedProcedureName, out string message)
        {
            DataSet dataSet = new DataSet();
            SqlCommand sqlCommand = new SqlCommand();

            message = "OK";

            this.openConnection();

            try
            {
                sqlCommand.CommandText = storedProcedureName;
                sqlCommand.CommandType = CommandType.StoredProcedure;
                sqlCommand.Connection = this.sqlConnection;
                SqlDataAdapter sqlDataAdapter = new SqlDataAdapter(sqlCommand);
                sqlDataAdapter.Fill(dataSet);
            }
            catch (Exception exception1)
            {
                Exception exception = exception1;
                message = exception.Message;
                dataSet = this.getDataSetMessage(exception.Message);
            }

            return dataSet;
        }

        private DataSet getDataSetMessage(string message)
        {
            DataTable dataTable = new DataTable("DataTableMessage");
            DataSet dataSet = new DataSet();
            dataSet.Clear();
            DataColumn dataColumn = new DataColumn("ErrorMessage", Type.GetType("System.String"));
            dataTable.Columns.Add(dataColumn);
            DataRow dataRow = dataTable.NewRow();
            dataRow["ErrorMessage"] = message;
            dataTable.Rows.Add(dataRow);
            dataSet.Tables.Add(dataTable);
            DataSet dataSet1 = dataSet;
            return dataSet1;
        }

        private DataTable getDataTableMessage(string message)
        {
            DataTable dataTable = new DataTable("DataTableMessage");
            DataSet dataSet = new DataSet();
            dataSet.Clear();
            DataColumn dataColumn = new DataColumn("ErrorMessage", Type.GetType("System.String"));
            dataTable.Columns.Add(dataColumn);
            DataRow dataRow = dataTable.NewRow();
            dataRow["ErrorMessage"] = message;
            dataTable.Rows.Add(dataRow);
            DataTable dataTable1 = dataTable;
            return dataTable1;
        }

        private void openConnection()
        {
            bool state = this.sqlConnection.State != ConnectionState.Closed;
            if (!state)
            {
                this.sqlConnection.ConnectionString = this.connString;
                this.sqlConnection.Open();
            }
        }
    }

    public interface ISQLaccess
    {
        NonQueryMessage execNonQueryString(string sqlString);
        void DBSQLsetup(string connString);
        void Dispose();
        DataSet executeStoredProcedure(string storedProcedureName, List<SqlParameter> sqlparams, out string message);
    }
}
