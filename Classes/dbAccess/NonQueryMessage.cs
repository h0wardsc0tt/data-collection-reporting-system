﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace SQLaccess
{
    [Serializable]
    public struct NonQueryMessage
    {
        private string message;

        private int rowsaffected;

        public NonQueryMessage(int rowsaffected, string message)
        {
            this.rowsaffected = rowsaffected;
            this.message = message;
        }

        public string Message
        {
            get
            {
                string str = message;
                return str;
            }

            set
            {
                message = value;
            }
        }

        public int RowsAffected
        {
            get
            {
                int num = this.rowsaffected;
                return num;
            }

            set
            {
                this.rowsaffected = value;
            }
        }
    }
}
