﻿using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.Options;
using System;
using System.Collections.Generic;
using System.Configuration;
using System.IO;
using System.Security.Cryptography;
using System.Text;
using WebApplication3;
using Newtonsoft.Json;

namespace Encrypt
{
    [Serializable]
    public class Encryptor : IEncryptionService
    {
        private const string PassPhrase = "GPp@s$Phr@$3";
        private const string HashAlgorithm = "SHA1";            // can be "MD5"
        private const int PasswordIterations = 5;               // can be any number
        private const string InitVector = "@1B2c3D4e5F6g7H8";   // must be 16 bytes
        private const int KeySize = 256;                        // can be 192 or 128
        private const string DefaultSalt = "Gsdf4!/Kg>ioW3Dh&9fQ?[}!";

        public Encryptor()
        {
        }
        public string Encrypt(string plainText, bool config)
        {
             return DoEncrypt(plainText, !config ? DefaultSalt : GetConfSettings("EncryptorSettings:Salt"));
        }
        public string Encrypt(string plainText, string salt)
        {
            return DoEncrypt(plainText, salt);
        }
        public string Decrypt(string plainText, bool config)
        {
            return DoDecrypt(plainText, !config ? DefaultSalt : GetConfSettings("EncryptorSettings:Salt"));
        }
        public string Decrypt(string plainText, string salt)
        {
            return DoDecrypt(plainText, salt);
        }
        private string DoEncrypt(string plainText, string saltValue)
        {
            byte[] initVectorBytes = Encoding.ASCII.GetBytes(InitVector);
            byte[] saltValueBytes = Encoding.ASCII.GetBytes(saltValue);

            byte[] plainTextBytes = Encoding.UTF8.GetBytes(plainText);

            PasswordDeriveBytes password = new PasswordDeriveBytes(
                                                            PassPhrase,
                                                            saltValueBytes,
                                                            HashAlgorithm,
                                                            PasswordIterations);

            byte[] keyBytes = password.GetBytes(KeySize / 8);
            RijndaelManaged symmetricKey = new RijndaelManaged();

            symmetricKey.Mode = CipherMode.CBC;

            ICryptoTransform encryptor = symmetricKey.CreateEncryptor(
                                                             keyBytes,
                                                             initVectorBytes);

            MemoryStream memoryStream = new MemoryStream();

            CryptoStream cryptoStream = new CryptoStream(
                memoryStream,
                encryptor,
                CryptoStreamMode.Write);

            // Start encrypting.
            cryptoStream.Write(plainTextBytes, 0, plainTextBytes.Length);

            // Finish encrypting.
            cryptoStream.FlushFinalBlock();

            // Convert our encrypted data from a memory stream into a byte array.
            byte[] cipherTextBytes = memoryStream.ToArray();

            // Close both streams.
            memoryStream.Close();
            cryptoStream.Close();

            // Convert encrypted data into a base64-encoded string.
            string cipherText = Convert.ToBase64String(cipherTextBytes);

            // Return encrypted string.
            return cipherText.Replace("+", "plus");
        }

        private string DoDecrypt(string cipherText, string saltValue)
        {
            cipherText = cipherText.Replace("plus", "+");
            byte[] initVectorBytes = Encoding.ASCII.GetBytes(InitVector);
            byte[] saltValueBytes = Encoding.ASCII.GetBytes(saltValue);

            // Convert our ciphertext into a byte array.
            byte[] cipherTextBytes = Convert.FromBase64String(cipherText);

            PasswordDeriveBytes password = new PasswordDeriveBytes(
                                                            PassPhrase,
                                                            saltValueBytes,
                                                            HashAlgorithm,
                                                            PasswordIterations);

            byte[] keyBytes = password.GetBytes(KeySize / 8);

            // Create uninitialized Rijndael encryption object.
            RijndaelManaged symmetricKey = new RijndaelManaged();

            // It is reasonable to set encryption mode to Cipher Block Chaining
            // (CBC). Use default options for other symmetric key parameters.
            symmetricKey.Mode = CipherMode.CBC;

            // Generate decryptor from the existing key bytes and initialization 
            // vector. Key size will be defined based on the number of the key 
            // bytes.
            ICryptoTransform decryptor = symmetricKey.CreateDecryptor(
                                                             keyBytes,
                                                             initVectorBytes);

            // Define memory stream which will be used to hold encrypted data.
            MemoryStream memoryStream = new MemoryStream(cipherTextBytes);

            // Define cryptographic stream (always use Read mode for encryption).
            CryptoStream cryptoStream = new CryptoStream(
                memoryStream,
                decryptor,
                CryptoStreamMode.Read);

            // Since at this point we don't know what the size of decrypted data
            // will be, allocate the buffer long enough to hold ciphertext;
            // plaintext is never longer than ciphertext.
            byte[] plainTextBytes = new byte[cipherTextBytes.Length];

            // Start decrypting.
            int decryptedByteCount = cryptoStream.Read(
                plainTextBytes,
                0,
                plainTextBytes.Length);

            // Close both streams.
            memoryStream.Close();
            cryptoStream.Close();

            // Convert decrypted data into a string. 
            // Let us assume that the original plaintext string was UTF8-encoded.
            string plainText = Encoding.UTF8.GetString(
                plainTextBytes,
                0,
                decryptedByteCount);

            // Return decrypted string.   
            return plainText;
        }

        private string GetConfSettings(string config)
        {
             return AppSettings.Instance.Get<string>(config);
        }
    }
    public interface IEncryptionService
    {
        string Encrypt(string plainText, bool config);
        string Encrypt(string plainText, string salt);
        string Decrypt(string plainText, bool config);
        string Decrypt(string plainText, string salt);
    }

}
